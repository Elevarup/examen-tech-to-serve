<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

///Posible home
Route::get('/home','HomeController@home');

///lista de los perros
Route::get('/api/perro','HomeController@list');

///Creación de un perro
Route::post('/api/perro/add','HomeController@storage')->middleware('age');

///explicación
Route::get('/api/explicacion','HomeController@explicacion');