<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Perro extends Model {

    /** @var string $table */
    protected $table ='perros';


    protected $fillable=['nombre','edad'];

    public $timestamps = false;
}
