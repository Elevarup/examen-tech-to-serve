<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

class AgeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($request->edad) &&is_numeric($request->edad) && $request->edad<0){
            return new JsonResponse(array('message'=>'No se registrar el perrito, es muy chiquito.'),JsonResponse::HTTP_BAD_REQUEST);
        }
         return $next($request);
    }
}
