<?php

namespace App\Http\Controllers;

use App\Entity\Perro;
use App\Http\Requests\PerroCreateRequest;
use Illuminate\Http\JsonResponse as HttpJsonResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends Controller
{

    public function __construct() { }


    public function  home(){
        $data = array(
            'nombre'=>'Luis Vicente',
            'apellido'=>'Arevalo Rios'
        );

        return new JsonResponse($data,JsonResponse::HTTP_OK);
    }

    public function storage(PerroCreateRequest $request){
        $dog = new Perro($request->all());

        $dog->save();
        return new JsonResponse(
            array( 
                'message'=>'Perro registrado satisfactoriamente.',
                'data'=>$dog),
            JsonResponse::HTTP_CREATED
        );
    } 

    public function list(){
        $listDog = Perro::all();
        return new JsonResponse($listDog,JsonResponse::HTTP_OK);
    }
    
    public function explicacion(){
        $texto = "Estandar abierto de autorización de APIs, "
            ."que nos permite compartir información entre sitios "
            .", sin tener que compartir la identidad. "
            ."Roles a tener en cuenta: Dueño del recurso,Cliente, servidor de recursos compartidos y servidor de autorización"
            ."En Laravel OAUTH2 se usa con el módulo: Passport.";
        return new HttpJsonResponse($texto,JsonResponse::HTTP_OK);
    }
}