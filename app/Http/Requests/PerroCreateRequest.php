<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\JsonResponse;

use Illuminate\Contracts\Validation\Validator;
class PerroCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'required',
            'edad'=>'required|numeric'
        ];
    }
    /**
     *
     * @return array
     */
    public function messages(){
        return[
            'nombre.required'=>'Parámetro "nombre" es obligatorio.',
            'edad.required'=>'Parámetro "edad" es obligatorio.',
            'edad.numeric'=>'Párametro "edad" no es numérico.'
        ];
    }


    protected function failedValidation(Validator $validator): void
    {
        $jsonResponse = response()->json( $validator->errors(), 422);

        throw new HttpResponseException($jsonResponse);
    }
}
